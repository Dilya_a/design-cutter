// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ СТАРТ
		
		$(function(){
			var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
			ua = navigator.userAgent,

			gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

			scaleFix = function () {
				if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
					viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
					document.addEventListener("gesturestart", gestureStart, false);
				}
			};
			
			scaleFix();
		});
		var ua=navigator.userAgent.toLocaleLowerCase(),
		 regV = /ipod|ipad|iphone/gi,
		 result = ua.match(regV),
		 userScale="";
		if(!result){
		 userScale=",user-scalable=0"
		}
		document.write('<meta name="viewport" id="myViewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

		// ============================================================
		//  window.onload = function () {
		// 	if(screen.width <= 617) {
		// 	    var mvp = document.getElementById('myViewport');
		// 	    mvp.setAttribute('content','width=617');
		// 	}
		// }
		// ============================================================

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ КОНЕЦ




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/




		var tabs = $(".tabs"),
			matchheight = $("[data-mh]"),
		    styler = $(".styler"),
		    owl = $(".owl-carousel"),
		    owlGallery = $(".owl-carousel_gallery")
		    flex = $(".flexslider"),
		    royalslider = $(".royalslider"),
			wow = $(".wow"),
			popup = $("[data-popup]"),
			superfish = $(".superfish"),
			windowW = $(window).width(),
			windowH = $(window).height();

			if(matchheight.length){
					include("plugins/jquery.matchHeight-min.js");
			}
			if(styler.length){
					include("plugins/formstyler/formstyler.js");
			}
			if(royalslider.length){
					include("plugins/royalslider/jquery.royalslider.min.js");
			}
			if(tabs.length){
					include("plugins/easy-responsive-tabs/easyResponsiveTabs.js");
			}
			if(wow.length){
					include("plugins/wow.min.js");
			}
			if(popup.length){
					include('plugins/arcticmodal/jquery.arcticmodal.js');
			}
			if(flex.length){
					include('plugins/flexslider/jquery.flexslider.js');
			}
			if(owl.length || owlGallery.length){
					include('plugins/owl-carousel/owl.carousel.js');
			}
			if(superfish.length){
					include('plugins/superfish/superfish.js');
					// include('plugins/superfish/hoverIntent.js');
			}

					include("plugins/modernizr.js");



			function include(url){ 

					document.write('<script src="'+ url + '"></script>'); 

			}

		


		$(document).ready(function(){




			/* ------------------------------------------------
			FORMSTYLER START
			------------------------------------------------ */

					if (styler.length){
						styler.styler({
							// selectSmartPositioning: true
						});
					}

			/* ------------------------------------------------
			FORMSTYLER END
			------------------------------------------------ */




			/* ------------------------------------------------
			FLEXSLIDER START
			------------------------------------------------ */

					if(flex.length){
						flex.flexslider({
						    animation: "slide",
						    controlNav: true,
							animationLoop: false,
							slideshow: false
						});
					}

			/* ------------------------------------------------
			FLEXSLIDER END
			------------------------------------------------ */




			/* ------------------------------------------------
			ROYALSLIDER START
			------------------------------------------------ */

					if(royalslider.length){
						royalslider.royalSlider({
						    fullscreen: false,
						    controlNavigation: 'bullets',
						    autoScaleSlider: true, 
						    autoScaleSliderWidth: 1872, 
						    autoScaleSliderHeight: 650,
						    loop: true,
						    imageScaleMode: 'none',
						    imageAlignCenter: false,
						    navigateByClick: true,
						    numImagesToPreload:2,
						    arrowsNav:true,
						    arrowsNavAutoHide: false,
						    arrowsNavHideOnTouch: true,
						    keyboardNavEnabled: true,
						    fadeinLoadedSlide: true,
						    sliderDrag:false,
						    globalCaption: false,
						    globalCaptionInside: false,
						    imgWidth: 1872,
						    transitionType:'move',
						    autoPlay: {
						      enabled: true,
						      pauseOnHover: false
						    },
						    block: {
						      delay: 400
						    },
						    bullets: {
						      controlsInside : true
						    },
						    thumbs: {
						      appendSpan: false,
						      firstMargin: true,
						      paddingBottom: 0,
						      fitInViewport:false,
						      spacing: 5
						    }
						});
					}

			/* ------------------------------------------------
			ROYALSLIDER END
			------------------------------------------------ */




			/* ------------------------------------------------
			OWL START
			------------------------------------------------ */

					if(owl.length){
						owl.owlCarousel({
							singleItem : true,
							items : 1,
							// loop: true,
							smartSpeed:1000,
							nav: true
							// autoHeight:true
						});
					}

					if(owlGallery.length){
					    owlGallery.each(function(){
					    	var $this = $(this),
					      		items = $this.data('items');

					    	$this.owlCarousel({
					    		singleItem : true,
								items : 3,
								// loop: true,
								smartSpeed:1000,
								// autoHeight:true,
					    		dots:true,
					    		nav: true,
					            navText: [ '', '' ],
					            margin: 1,
					            // responsive : items,
					            responsive : {
								    // breakpoint from 0 up
								    0 : {
								        items: 1,
								    },
								    // breakpoint from 480 up
								    480 : {
								        items: 2,
								        margin: 30,
								    },
								    // breakpoint from 768 up
								    992 : {
								        margin: 30,
								        items: 3,
								    }
								}
					    	});
					    });
					}
					// <div class="owl-carousel" data-items='{  "0":{"items":1},   "480":{"items":2},   "991":{"items":3}  }'></div>

			/* ------------------------------------------------
			OWL END
			------------------------------------------------ */




			/* ------------------------------------------------
			TABS START
			------------------------------------------------ */

					if(tabs.length){
						tabs.easyResponsiveTabs();
					}

			/* ------------------------------------------------
			TABS END
			------------------------------------------------ */




			/* ------------------------------------------------
			ANIMATE block START
			------------------------------------------------ */

					if(wow.length){
				        if($("html").hasClass("md_no-touch")){
							new WOW().init();	
						}
						else if($("html").hasClass("md_touch")){
							$("body").find(".wow").css("visibility","visible");
						}

					}

			/* ------------------------------------------------
			ANIMATE block END
			------------------------------------------------ */




			/* ------------------------------------------------
			POPUP START
			------------------------------------------------ */

					if(popup.length){
						popup.on('click',function(){
						    var modal = $(this).data("popup");
						    $(modal).arcticmodal();
						});
					};

			/* ------------------------------------------------
			POPUP END
			------------------------------------------------ */




			/* ------------------------------------------------
			SUPERFISH START
			------------------------------------------------ */

					if(superfish.length){
						superfish.superfish();
					}

			/* ------------------------------------------------
			SUPERFISH END
			------------------------------------------------ */

		});




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================
